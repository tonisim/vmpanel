import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<boolean> {
    return this.http.post<{token: string}>('http://localhost:3000/authenticate', {name: username, password: password})
      .pipe(
        map(result => {
          localStorage.setItem('x-access-token', result.token);
          return true;
        })
      );
  }

  logout() {
    localStorage.removeItem('x-access-token');
  }

  public get loggedIn(): boolean {
    return (localStorage.getItem('x-access-token') !== null);
  }
}