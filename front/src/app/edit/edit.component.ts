import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  @Input() postData: any =  { ip:'', disksize: '', memory:'', operatingsystem:'',osversion:'', status:'ok'};

  constructor(private  ApiService:  ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.ApiService.getPost(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.postData = data;
    });
  }
  updatePost() {
    this.ApiService.updatePost(this.route.snapshot.params['id'], this.postData).subscribe((result) => {
      this.router.navigate(['']);
    }, (err) => {
      console.log(err);
    });
  }
}
