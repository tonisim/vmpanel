import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  @Input() postData = { ip:'', disksize: '', memory:'', operatingsystem:'',osversion:'', status:'ok'};

  constructor(private  ApiService:  ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }
  addPost() {
    console.log(this.postData);
    this.ApiService.addPost(this.postData).subscribe((result) => {
      this.router.navigate(['']);
    }, (err) => {
      console.log(err);
    });
  }
  updatePost() {
    this.ApiService.updatePost(this.route.snapshot.params['id'], this.postData).subscribe((result) => {
      this.router.navigate(['/edit/'+this.route.snapshot.params['id']]);
    }, (err) => {
      console.log(err);
    });
  }
}
