import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from  '@angular/common/http';

import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const API_URL = "http://localhost:3000";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public id: string;
  
  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getPosts(): Observable<any> {
    return this.http.get(API_URL + '/vm').pipe(
      map(this.extractData));
  }

  getPost(id): Observable<any> {
    return this.http.get(API_URL + '/vm/' + id).pipe(
      map(this.extractData));
  }

  addPost (post): Observable<any> {
    // return this.http.post(API_URL +'/vm/add', post)
    console.log(post);
    return this.http.post<any>(API_URL + '/vm/add', post, httpOptions).pipe(
      tap((post) => console.log(`added post w/ id=${post.id}`)),
      catchError(this.handleError<any>('addPost'))
    );
  }

  deletePost (id): Observable<any> {
    return this.http.delete<any>(API_URL + '/vm/' + id, httpOptions).pipe(
      tap(_ => console.log(`deleted post id=${id}`)),
      catchError(this.handleError<any>('deletePost'))
    );
  }
  updatePost (id, post): Observable<any> {
    return this.http.put(API_URL + '/vm/' + id, JSON.stringify(post), httpOptions).pipe(
      tap(_ => console.log(`updated post id=${id}`)),
      catchError(this.handleError<any>('updatePost'))
    );
  }

private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    console.log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}
}
