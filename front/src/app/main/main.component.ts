import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  public id: string;
  interval : any;
  posts:any = [];

  constructor(private  ApiService:  ApiService, private route: ActivatedRoute, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    this.getPosts();
  }

  getPosts() {
    this.posts = [];
    this.ApiService.getPosts().subscribe((data: {}) => {
      console.log(data);
      this.posts = data;
    });
  }
  delete(id) {
    this.ApiService.deletePost(id)
      .subscribe(res => {
          this.getPosts();
        }, (err) => {
          console.log(err);
        }
      );
  }

  logout() {
    this.auth.logout();
    this.router.navigate(['']);
  }
}
