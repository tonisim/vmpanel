var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');

VirtualMachine = require('./models/virtualmachine');
User = require('./models/user');
var config = require('./config');

mongoose.connect('mongodb://localhost/vmpanel');
var db = mongoose.connection;

app.set('superSecret', config.secret); 

app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.get('/', function(req, res){
    res.send('test');
});

app.get('/vm/', function(req, res){
    VirtualMachine.getAllMachines(function(err, virtualmachines){
        if(err){
            throw err;
        }
        res.json(virtualmachines);
    });
});

app.get('/vm/:id', function(req, res){
    VirtualMachine.getMachineById(req.params.id, function(err, virtualmachine){
        if(err){
            throw err;
        }
        res.json(virtualmachine);
    });
});

app.post('/vm/add', /* checkToken, */ function(req, res){
    var virtualmachine = req.body;
    VirtualMachine.addMachine(virtualmachine, function(err, virtualmachine){
        if(err){
            throw err;
        }
        res.json(virtualmachine);
    });
});

app.put('/vm/:id', /* checkToken, */ function(req, res){
    var virtualmachine = req.body;
    VirtualMachine.updateMachine(req.params.id, virtualmachine, function(err, virtualmachine){
        if(err){
            throw err;
        }
        res.json(virtualmachine);
    });
});

app.delete('/vm/:id', /* checkToken, */ function(req, res){
    VirtualMachine.deleteMachine(req.params.id, function(err, virtualmachine){
        if(err){
            throw err;
        }
        res.json(virtualmachine);
    });
});

/* app.post('/authenticate', function(req, res){
    User.findOne({
        name: req.body.name
    }, function(err, user) {

        if (err) throw err;

        if (!user) {
        res.json({ success: false, message: 'Authentication failed. User not found.' });
        } else if (user) {

        if (user.password != req.body.password) {
            res.json({ success: false, message: 'Authentication failed. Wrong password.' });
        } else {

        const payload = {
            admin: user.admin 
        };

        var token = jwt.sign(payload, app.get('superSecret'));

        res.json({
            success: true,
            token: token
        });
        }   

        }

    });
});

function checkToken(req, res, next) {
    let token = req.headers['x-access-token'] || req.headers['authorization']; 
    // Express headers are auto converted to lowercase
    if (token) {
        if (token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length).trimLeft();
        } else {
            return res.status(403).send({
              success: false,
              message: 'No token provided.'
            });
          }
    }
    if (token) {
      jwt.verify(token, app.get('superSecret'), function(err, decoded) {
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {
          req.decoded = decoded;
          next();
        }
      });
    } else {
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });
    }
}; */


app.listen(3000);