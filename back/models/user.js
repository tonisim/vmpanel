var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    name: String, 
    password: String,
    admin: Boolean
});

var User = module.exports = mongoose.model('User', userSchema);