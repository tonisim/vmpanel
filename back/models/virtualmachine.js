var mongoose = require('mongoose');

var virtualMachineSchema = mongoose.Schema({
    ip:{
        type: String,
        required: true
    },
    disksize:{
        type: String,
        required: true
    },
    memory:{
        type: String,
        required: true
    },
    operatingsystem:{
        type: String,
        required: true
    },
    osversion:{
        type: String,
        required: true
    },
    createdtime:{
        type: Date,
        default: Date.now
    },
    status:{
        type: String,
        required: true
    }
});

var VirtualMachine = module.exports = mongoose.model('VirtualMachine', virtualMachineSchema);

module.exports.getAllMachines = function(callback){
    VirtualMachine.find(callback);
};

module.exports.getMachineById = function(id, callback){
    VirtualMachine.findById(id,callback);
};

module.exports.addMachine = function(virtualmachine, callback){
    VirtualMachine.create(virtualmachine, callback);
};

module.exports.updateMachine = function(id, virtualmachine, callback){
    VirtualMachine.findByIdAndUpdate(id, virtualmachine, callback);
};

module.exports.deleteMachine = function(id, callback){
    VirtualMachine.findByIdAndRemove(id, callback);
};